# SwiftBoardGameLibrary #

`SwiftBoardGameLibrary` is a simple *two player board game* framework implemented in Swift. It comes with  a *Connect4* sample. So you can learn how to use the framework and how to implement your own board games. The library provides a simple alpha-beta search engine for game tree analysis. 


## How to setup your Project ##
This section steps you through the process of using the *Swift Package Manager*  to create a new project from scratch and import the `SwiftBoardGameLibrary`.   

First generate a new executable project by creating a new directory called `MyBoardGameProject`. The following command uses the name of the current working directory for automatically creating a new project with that name:

```Bash
~ $ cd SwiftConnectFourApp/
~/SwiftConnectFourApp $ swift package init --type executable

Creating executable package: SwiftConnectFourApp
Creating Package.swift
Creating .gitignore
Creating Sources/
Creating Sources/main.swift
Creating Tests/

```

The `Package.swift` file is a manifest file containing the name of the package and other information such as the dependencies that your project has. Alter this file so that it contains the dependency to the *SwiftBoardGameLibrary*.

```Swift
import PackageDescription

let package = Package(
    name: "SwiftConnectFourApp"
    dependencies: [
       .Package(url: "https://bitbucket.org/thomaskausch/swiftboardgamelibrary.git", 
       majorVersion: 1, minor: 0)
    ]
)    
```

Now produce an executable file from your project by running `swift build`:

```Bash
~/SwiftConnectFourApp$ swift build
Cloning https://bitbucket.org/thomaskausch/swiftboardgamelibrary.git
HEAD is now at 264994d Initial release
Resolved version: 1.0.1
Compile Swift Module 'SwiftBoardGameLibrary' (7 sources)
Compile Swift Module 'SwiftConnectFourApp' (1 sources)
Linking ./.build/debug/SwiftConnectFourApp

```
The swift build command reads the `Package.swift`file and checks if there are any dependenciues to fetch. Next it runs the Swift compiler and links the `.swiftmodule` files to produce your executable, which is left in the `.build/debug` directory. 


## Developing in XCode
While you can easily develp entierly on the command line using `swift`and your favorite text editor, most developers want to use XCode for their Swift development. In order to use XCode, a project must be created from your `Package.swift`. The following command will create your project:

```Bash
$ swift package generate-xcodeproj
```

Running this command line fetches the necessary dependencies before creating the XCoce project. Note: When you build with Xcode, it does not use SwiftPM to build your project, but instead will use the Xcode native build system.

If you add or remove a dependency to your `Package.swift` file, it will be necessary for you to recreate the Xcode project again. Note that any changes made to your project will be lost when a new project file is generated over the old one. 

## Steps to create your board game ##
You can implement your own board game in two steps:

1. Create your *Move* class
2. Create your *Board* class

The tricky part is to implement your own *Board* class that implements the following protocol:

```swift
public protocol Board  {    
    /// Returns all valid moves for current position.
    var validMoves: [MoveType] { get }
  
    /// Returns heuristic board value.
    var heuristicValue: Int { get }
    
    /// Returns player who is next to move.
    var next: Player { get }
    
    /// Returns winner of the board or nil if game is not yet finished.
    var winner: Player? { get }
    
    /// Returns true if game is over, false otherwise.
    var isEndPosition: Bool { get }
    
    /// Returns true if board is searching.
    var searching: Bool { get set}
    
    /// Returns true if move is valid.
    func isValid(move: MoveType) -> Bool

    /// Executes move on current board.
    func execute(move: MoveType)
    
    /// Undo move on current board.
    func undo(move: MoveType)
    
    /// Reset board to initial position
    func newGame()
}
```

Especially the *heuristicValue* property needs special attention. It is used to evaluate a 
board position when the search engine has reached the search depth but the board does not
represent end position. In this case the search engine needs a hint if it is a good position for white (the maximizer) or black (the minimizer). Therefore return a large positive integer if the board represents a good position for white or a negative value if it is a better position for black. Your heuristic function can be high sophisticated - however remember it must be evaluated thousands of times during search tree analysis. So the function's time complexity should be as good as possible. 

Another important computed property is the *validMoves*. It should  return all valid moves of the board. You can increase the search engine performance if you return the best move first - however this is often not a simple task. Have a look at *ConnectBoard* class it will give you a good sample of how this can be done. 

And last but not least don't forget your Board Unit Tests. You will love it!

## How to choose a heuristic Board Value ##
The used heuristic for *ConnectBoard* uses the fact that it is better to have a stone in the middle columns than on the edge columns. It uses the column weights `[10, 20, 40, 80, 40, 20, 10]` for each stone and takes the sum over all stones on the board  where a white stone has positiv weight and a black stone a negative one. This sum can be adjusted with only one addition for each move and is therefore very efficient!


## About the project ##

This project is the result of my Test-Driven-Development (TDD) experiment in November 2015 where I tried to implement the model and search engine completely with TDD. 