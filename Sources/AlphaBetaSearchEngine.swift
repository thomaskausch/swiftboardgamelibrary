//
//  SearchEngine.swift
//  SwiftBoardGameLibrary
//
//  Created by Thomas Kausch on 09/08/14.
//  Copyright (c) 2014 Thomas Kausch. All rights reserved.
//
//
//  SwiftBoardGameLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General   License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwiftBoardGameLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General   License for more details.
//
//  You should have received a copy of the GNU General   License
//  along with SwiftBoardGameLibrary.  If not, see <http://www.gnu.org/licenses/>.
//

import Foundation

public class AlphaBetaSearchEngine<B: Board>: SearchEngine {
    
    public var numberOfAlphaBetaCuts: Int = 0
    public var numberOfVisitedBoards: Int = 0
    
    public init() {
    }
    
    public func bestMove(board: inout B, searchDepth: Int ) -> (move: B.MoveType?, value: Int) {
        
        numberOfVisitedBoards = 0
        numberOfAlphaBetaCuts = 0
        
        board.searching = true
        let result = alphaBetaSearch(board: board, depth: searchDepth, alpha: Int.min, beta: Int.max)
        board.searching = false
        
        return result
    }
    
    func alphaBetaSearch(board: B, depth: Int, alpha: Int, beta: Int) -> (move: B.MoveType?, value: Int) {
        
        numberOfVisitedBoards += 1
        
        if board.isEndPosition || depth <= 0 {
            return (nil, board.heuristicValue)
        } else {
            
            // Precondition: There are valid moves and search depth is greater zero
            
            if board.next == .white {
                
                // Precondition: White is moving and there is at least one move
                var bestValue = Int.min
                var bestMove : B.MoveType?
                var alphaNext = alpha
                
                for move in board.validMoves {
                    board.execute(move: move)
                    
                    let nextMove = alphaBetaSearch(board: board, depth: depth - 1, alpha: alphaNext, beta: beta)
                    
                    // we found better move so adjust variables
                    if nextMove.value >= bestValue  {
                        bestValue = nextMove.value
                        bestMove = move
                        alphaNext = max(alphaNext,bestValue)
                    }
                    
                    board.undo(move: move)
                    
                    // Beta Cutoff if  move value is greater than the best move value of 
                    // my father Minimizer node (beta). The minimizer would never choose
                    // this path in game tree!
                    if beta <= alphaNext  {
                        break
                    }
                    
                }
                
                return (bestMove, bestValue)
                
                
            } else {
                
                // Precondition: Black is moving and there is at least one move
                var bestValue = Int.max
                var bestMove : B.MoveType?
                var betaNext = beta
                
                for move in board.validMoves {
                    board.execute(move: move)
                    
                    let nextMove = alphaBetaSearch(board: board, depth: depth - 1, alpha: alpha, beta: betaNext)
                    
                    // we found better move so adjust variables
                    if nextMove.value <= bestValue  {
                        bestValue = nextMove.value
                        bestMove = move
                        betaNext = min(betaNext,bestValue)
                    }
                    
                    board.undo(move: move)
                    
                    // Alpha Cutoff if  move value is smaller than the best move value of
                    // my father Maximizer node (beta). The minimizer would never choose
                    // this path in game tree!
                    if betaNext <= alpha  {
                        break
                    }
                
                }
                
                return (bestMove, bestValue)
                
            }
            
        }
        
    }
    
    
    
    
}
