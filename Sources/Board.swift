//
//  Board.swift
//  SwiftBoardGameLibrary
//
//  Created by Thomas Kausch on 08/08/14.
//  Copyright (c) 2014 Thomas Kausch. All rights reserved.
//
//
//  SwiftBoardGameLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General   License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwiftBoardGameLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General   License for more details.
//
//  You should have received a copy of the GNU General   License
//  along with SwiftBoardGameLibrary.  If not, see <http://www.gnu.org/licenses/>.
//
import Foundation

/// A Board defines all methods and properties used for any two player
/// game.
public protocol Board  {

    associatedtype MoveType: Move
    
    /// Returns all valid moves for current position.
    var validMoves: [MoveType] { get }
    
    /// Returns heuristic board value.
    var heuristicValue: Int { get }
    
    /// Returns player who is next to move.
    var next: Player { get }
    
    /// Returns winner of the board or nil if game is not yet finished.
    var winner: Player? { get }
    
    /// Returns true if game is over, false otherwise.
    var isEndPosition: Bool { get }
    
    /// Returns true if board is searching.
    var searching: Bool { get set}
    
    /// Returns true if move is valid.
    func isValid(move: MoveType) -> Bool

    /// Executes move on current board.
    func execute(move: MoveType)
    
    /// Undo move on current board.
    func undo(move: MoveType)
    
    /// Reset board to initial position
    func newGame()
 
}
