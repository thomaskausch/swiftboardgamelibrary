//
//  ConnectBoard.swift
//  SwiftBoardGameLibrary
//
//  Created by Thomas Kausch on 22/08/14.
//  Copyright (c) 2014 Thomas Kausch. All rights reserved.
//
//
//  SwiftBoardGameLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General   License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwiftBoardGameLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General   License for more details.
//
//  You should have received a copy of the GNU General   License
//  along with SwiftBoardGameLibrary.  If not, see <http://www.gnu.org/licenses/>.
//

import Foundation



//    ConnectBoard class is using the following matrix to represent a connect board:
//
//       - | - - - - - - - | -
//       –––––––––––––––––––––
//       - | - - - - - - - | -
//       - | - - - - - - - | -
//       - | - - - - - - - | -
//       - | - - - - - - - | -
//       - | - 0 - - - - - | -
//       - | x 0 x - - - - | -
//       –––––––––––––––––––––
//       - | - - - - - - - | -
//
//      where x = .white, 0 = .black and - = nil.
//
//      Note: There is an extra row and column.
//      This makes it much easier to determine if game ist over. We don't have to check
//      for crossing the matrix borders.
//
//      This matrix is mapped to a one dimensional array named cells which makes
//      computations much faster.
public protocol ConnectBoardDelegate {
    func cellDidChange(row: Int, col: Int)
    func endOfGame(row: Int, col: Int, direction: Int)
}

public class ConnectBoard: Board  {
    
    /// Connect board column count
    public static let Columns  = 7
    /// Connect board row count
    public static let Rows = 6
    
    public var next = Player.white
    public var winner: Player? = nil
    public var searching = false
    
    // Derived constants
    private let cellCount: Int
    private let columnWeights: [Int]
    private let directions: [Int]
    
    // Board state
    public var delegate: ConnectBoardDelegate?;
    private var shuffledColumns = [4,3,5,2,6,1,7]
    fileprivate var cells: [Player?]
    fileprivate var topRowForColumn: [Int]
    private var moveCount = 0
    private var weightSum = 0
    
    
    // MARK: - Initialization methods
    
    public convenience init() {
        self.init(delegate: nil)
    }
    
    public init(delegate: ConnectBoardDelegate?) {
        
        self.delegate = delegate;
        
        // initialize cells
        cellCount = (ConnectBoard.Columns + 2) * (ConnectBoard.Rows + 2)
        cells = [Player?](repeating: nil,  count: cellCount)
        
        // top row index for each column
        topRowForColumn = [0, 1, 1, 1, 1, 1, 1, 1, 0]
        
        // initialize weights for each column
        columnWeights = [0, 10, 20, 40, 80, 40, 20, 10, 0]
        // LeftUp, Up, RightUp, Right
        directions = [8, 9, 10, 1]
        
    }
    
    public func newGame() {
        winner = nil
        next = .white
        cells = [Player?](repeating: nil,  count: cellCount)
        topRowForColumn = [0, 1, 1, 1, 1, 1, 1, 1, 0]
        weightSum = 0
        moveCount = 0
        searching = false
    }
    
    public var isEndPosition: Bool {
        return winner != nil
    }
    
    
    
    // MARK: - Board methods
    
    public var validMoves: [ConnectMove] {
        var moves = [ConnectMove]();
        if !isEndPosition {
            shuffle(&shuffledColumns)
            for column in shuffledColumns  {
                if !isColumnFull(column: column) {
                    moves.append(ConnectMove(player: next, column: column))
                }
            }
        }
        return moves;
    }
    
    public func isValid(move: ConnectMove) -> Bool {
        if !isEndPosition {
            return move.player == next && !isColumnFull(column: move.column)
        } else {
            return false
        }
    }
    
    private func isColumnFull(column: Int) -> Bool {
        return topRowForColumn[column] >  ConnectBoard.Rows
    }
    
    public func execute(move: ConnectMove) {
        
        // Precondition: Move is valid
        
        // set board cell to move player and adjust invariants
        let moveRow = topRowForColumn[move.column]
        
        self[moveRow, move.column] = move.player
        
        moveCount += 1
        next = next.oponent()
        weightSum += move.player.rawValue * columnWeights[move.column]
        topRowForColumn[move.column] += 1
        
        checkForWinner(row: moveRow, column: move.column)
        
        if !searching {
            cellDidChange(row: moveRow, column: move.column)
        }
    }
    
    func checkForWinner(row: Int, column: Int)  {
        
        // Precondition: move is valid
        
        let cellIndex = row * (ConnectBoard.Columns + 2) + column
        let cellColor = cells[cellIndex]
        
        var counter = 0
        var foundDirection: Int?
        var foundCellIndex: Int?
        
        for direction in directions {
            
            // find end of line of cells with the same color/player
            var index = cellIndex
            repeat {
                index += direction
            } while cells[index] == cellColor
            
            // count number of cells with same color in a line
            counter = 0
            index -= direction
            while cells[index] == cellColor {
                counter += 1
                index -= direction
            }
            
            // Check if four in a row are detected - break loop
            if counter >= 4 {
                foundDirection = direction
                foundCellIndex = index + direction
                break
            }
        }
        
        // Check if we have found 4 in a line or full board
        if counter >= 4 {
            winner = cellColor
            if !searching {
                endOfGame(cellIndex: foundCellIndex!, direction: foundDirection!)
            }
        } else if moveCount == ConnectBoard.Rows * ConnectBoard.Columns {
            winner = nil
        }
        
    }
    
    public var heuristicValue: Int {
        if isEndPosition {
            if let player = winner {
                switch (player) {
                case .white:
                    return Int.max
                case Player.black:
                    return Int.min
                }
            } else {
                return 0
            }
        } else {
            return weightSum
        }
    }
    
    public func undo(move: ConnectMove) {
        
        let lastMoveRow = topRowForColumn[move.column] - 1
        
        self[lastMoveRow, move.column] = nil
        
        // adjust invariants
        topRowForColumn[move.column] -= 1
        moveCount -= 1
        next = next.oponent()
        weightSum -= move.player.rawValue * columnWeights[move.column]
        winner = nil
        
        cellDidChange(row: lastMoveRow, column: move.column)
        
        
    }
    
    func moveForColumn(_ column: Int) -> ConnectMove? {
        if (!isEndPosition && column <= ConnectBoard.Columns && topRowForColumn[column] <= ConnectBoard.Rows) {
            return ConnectMove(player: next, column: column)
        } else {
            return nil
        }
    }
    
    // MARK: - Helper methods
    
    private func shuffle( _ list: inout [Int]) {
        
        // Using Donald E. Knuth Algorithm to shuffle array in o(n) time
        for i in 0..<list.count  {
            let j = Int(arc4random_uniform(UInt32(list.count - i))) + i
            if i != j {
                swap(&list[i], &list[j])
            }
        }
        
    }
    
    // row: goes from 1 ... Rows
    // column: goes from 1 ... Columns
    subscript(row: Int, column: Int) -> Player? {
        get {
            return cells[row * (ConnectBoard.Columns + 2) + column]
        }
        set {
            cells[row * (ConnectBoard.Columns + 2) + column] = newValue
        }
    }
    
    
    // MARK: - Call delegate methods
    
    private func cellDidChange(row: Int, column: Int) {
        delegate?.cellDidChange(row: row, col: column)
    }
    
    
    
    private func endOfGame(cellIndex: Int, direction: Int) {
        
        let row = cellIndex / (ConnectBoard.Columns + 2)
        let column = cellIndex % (ConnectBoard.Columns + 2)
        let direction = direction == 1 ? 1 : direction - 2
        
        delegate?.endOfGame(row: row, col: column, direction: direction)
        
    }
    
    
    
}

/// Transform board to string for debugging purpose
extension ConnectBoard: CustomStringConvertible {
    
    public var description: String {
        var rtn = ""
        for row in 0...ConnectBoard.Rows - 1  {
            for col in 1...ConnectBoard.Columns {
                if let cellState = self[ConnectBoard.Rows - row,col] {
                    switch (cellState) {
                    case .white:
                        rtn += "X "
                    case Player.black:
                        rtn += "0 "
                    }
                } else {
                    rtn += "- "
                }
            }
            rtn += "\n"
        }
        return rtn
    }
    
}


/// Equatablility is used for testing
extension ConnectBoard: Equatable {
    
    public static func ==(lhs: ConnectBoard, rhs: ConnectBoard) -> Bool {
        
        if lhs.cells.count != rhs.cells.count {
            return false
        }
        
        for i in 0..<lhs.cells.count  {
            if lhs.cells[i] != rhs.cells[i] {
                return false
            }
        }
        
        if  lhs.next != rhs.next ||
            lhs.isEndPosition != rhs.isEndPosition ||
            lhs.winner != rhs.winner {
            return false
        }
        
        for i in 0..<lhs.topRowForColumn.count  {
            if lhs.topRowForColumn[i] != rhs.topRowForColumn[i] {
                return false
            }
        }
        
        return true
    }
    
}


