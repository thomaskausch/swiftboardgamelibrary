//
//  ConnectMove.swift
//  SwiftBoardGameLibrary
//
//  Created by Thomas Kausch on 27/10/15.
//  Copyright (c) 2014 Thomas Kausch. All rights reserved.
//
//
//  SwiftBoardGameLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General   License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwiftBoardGameLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General   License for more details.
//
//  You should have received a copy of the GNU General   License
//  along with SwiftBoardGameLibrary.  If not, see <http://www.gnu.org/licenses/>.
//
import Foundation

import Foundation

public struct ConnectMove: Move {
    
    public let player: Player
    public let column: Int
    
    public init(player: Player, column: Int) {
        self.player = player
        self.column = column
    }
    
}




extension ConnectMove: CustomStringConvertible {
    public var description: String {
        return "{ player = \(self.player), column = \(self.column) }"
    }
}

extension ConnectMove: Equatable {
    public static func ==(lhs: ConnectMove, rhs: ConnectMove) -> Bool {
        return lhs.player == rhs.player && lhs.column == rhs.column
    }
}
