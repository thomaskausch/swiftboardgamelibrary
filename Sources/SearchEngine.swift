//
//  GameTreeSearch.swift
//  SwiftBoardGameLibrary
//
//  Created by Thomas Kausch on 19/03/16.
//  Copyright © 2016 Swisscom. All rights reserved.
//
//  SwiftBoardGameLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General   License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwiftBoardGameLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General   License for more details.
//
//  You should have received a copy of the GNU General   License
//  along with SwiftBoardGameLibrary.  If not, see <http://www.gnu.org/licenses/>.
//
import Foundation


/// SearchEngine defines simple move search API
public protocol SearchEngine {
    
    associatedtype BoardType: Board
    
    /// Returns the total number of visited board positions during search
    var numberOfVisitedBoards: Int { get }
    
    /// Returns the best calculated move and its heuristic value.
    func bestMove(board: inout BoardType, searchDepth: Int ) -> (move: BoardType.MoveType?, value: Int)
    
}
