import XCTest
@testable import SwiftBoardGameLibraryTests

XCTMain([
     testCase(SwiftBoardGameLibraryTests.allTests),
])
