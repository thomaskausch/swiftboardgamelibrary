//
//  ConnectBoardTests.swift
//  SwiftBoardGameLibrary
//
//  Created by Thomas Kausch on 25/08/14.
//  Copyright (c) 2014 Thomas Kausch. All rights reserved.
//
//  SwiftBoardGameLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General   License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwiftBoardGameLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General   License for more details.
//
//  You should have received a copy of the GNU General   License
//  along with SwiftBoardGameLibrary.  If not, see <http://www.gnu.org/licenses/>.
//
import XCTest
@testable import SwiftBoardGameLibrary


class ConnectBoardTests: XCTestCase {

    var board = ConnectBoard()
    
    override func setUp() {
        super.setUp()
        self.board = ConnectBoard()
    }
    
    func testInit() {
        self.board = ConnectBoard()
        self.assertBoard(self.board)
    }
    
    
    
    func testUndoMove() {
   
        let emptyBoard = ConnectBoard()
        let move = ConnectMove(player:.white, column:1)

        board.execute(move: move)
        XCTAssertNotEqual(board, emptyBoard, "Board must have changed")
        board.undo(move: move)
        XCTAssertEqual(board, emptyBoard, "Board must be empty after undo")
        
    }
    
    
    
    func testValidMovesForBoardWithFullColumn() {
        
        //        0
        //        x
        //        0
        //        x
        //        0
        //        x
        
        for _ in 1...3 {
            self.board.execute(move: ConnectMove(player: .white, column: 4))
            self.board.execute(move: ConnectMove(player: .black, column: 4))
        }
        let validMoves = self.board.validMoves;
        self.assertMoves(validMoves, expectedColumns: [1,2,3,5,6,7], expectedPlayer: .white);
    }

    
    
    
    func testValidMovesForEndPositionBoard() {
    
        // x
        // x 0
        // x 0
        // x 0
        
        for _ in 1...3 {
            self.board.execute(move: ConnectMove(player: .white, column: 4))
            self.board.execute(move: ConnectMove(player: .black, column: 3))
        }
        self.board.execute(move: ConnectMove(player: .white, column: 4))

        XCTAssertTrue(self.board.validMoves.isEmpty)
        
    }
    
    func testValidMovesForDrawBoard() {
        
        // 0 x 0 x 0 x 0
        // 0 x 0 x 0 x 0
        // 0 x 0 x 0 x 0
        // x 0 x 0 x 0 x
        // x 0 x 0 x 0 x
        // x 0 x 0 x 0 x
        
        for whiteColumn in [1,3,5,7,2,4,6] {
            for _ in 0...2 {
                self.board.execute(move: ConnectMove(player: .white, column: whiteColumn))
                let blackColumn = whiteColumn + 1 == 8 ? 1 : whiteColumn + 1
                self.board.execute(move: ConnectMove(player: .black, column: blackColumn))
            }
        }
        
        XCTAssertTrue(self.board.validMoves.isEmpty)
    }
    
    func testdoMoveVertical() {
        
        // 0
        // x
        // 0
        // x
        // 0
        // x
        
        let col = 1
        
        for row in 1...5 {
            
            let player = row % 2 == 1 ? .white: Player.black
            
        
            self.board.execute(move: ConnectMove(player: player, column: col))
            
            XCTAssertEqual(board[row,col], player)
            XCTAssertFalse(board.isEndPosition)
            XCTAssertEqual(board.winner, nil)
            XCTAssertEqual(board.next, player.oponent())
            XCTAssertEqual(board.heuristicValue, player == Player.black ? 0 : 10)
        
        }
        
    }

    
    func testDoMoveHorizontal() {
        
        // 0 0 0
        // x x x x
        
        let expectedBoardValues = [10, 20, 40, 80];
        
        for col in 1...3 {
            
            self.board.execute(move: ConnectMove(player: .white, column: col))
            XCTAssertEqual(board[1,col], .white)
            XCTAssertFalse(board.isEndPosition)
            XCTAssertEqual(board.winner, nil)
            XCTAssertEqual(board.next, .black)
            XCTAssertEqual(board.heuristicValue, expectedBoardValues[col - 1])
            
            self.board.execute(move: ConnectMove(player: .black, column: col))
            XCTAssertEqual(board[2,col], .black)
            XCTAssertFalse(board.isEndPosition)
            XCTAssertEqual(board.winner, nil)
            XCTAssertEqual(board.next, .white)
            XCTAssertEqual(board.heuristicValue,0)
            
        }
        
        self.board.execute(move: ConnectMove(player: .white, column: 4))
        XCTAssertEqual(board[1,4], .white)
        XCTAssertTrue(board.isEndPosition)
        XCTAssertEqual(board.winner, .white)
        XCTAssertEqual(board.next, .black)
        XCTAssertEqual(board.heuristicValue, Int.max)
        
    }
 
    

    
    func testCheckEndPositionForInaColumn() {
        
        // x
        // x
        // x
        // x 0 0 0
        
        self.moveForColumns([1,2,1,3,1,4,1])
        
        XCTAssertTrue(self.board.isEndPosition)
        XCTAssertEqual(self.board.winner, .white)
        XCTAssertEqual(self.board.heuristicValue, Int.max)
        
        
    }
    
    
    func testCheckEndPositionFourInaDiagonal() {
        
        //       x
        //     x x
        //   x x 0
        // x 0 0 0   0
        
        self.moveForColumns([1,2,2,3,3,4,3,4,4,6,4]);
        
        XCTAssertTrue(self.board.isEndPosition)
        XCTAssertEqual(self.board.winner, .white)
        XCTAssertEqual(self.board.heuristicValue, Int.max)
        
    }
    
    func testNoEndPosition() {
        
        // Note: This was a bug in check end position...
        
        //
        // 0           0
        // x 0         0
        // x x 0 x x x 0
        
        self.moveForColumns([1,7,1,1,2,2,4,3,5,7,6,7]);
        
        XCTAssertTrue(!self.board.isEndPosition)
        XCTAssertEqual(self.board.winner, nil)
        XCTAssertNotEqual(self.board.heuristicValue, Int.max)
    }
    
    
    func moveForColumns(_ columns: [Int]) {
        for column in columns {
            let move = self.board.moveForColumn(column)
            self.board.execute(move: move!)
        }
    }
    
    
    func assertBoard(_ board: ConnectBoard) {
        XCTAssertEqual(board.next, .white)
        XCTAssertEqual(board.winner, nil)
        XCTAssertEqual(board.heuristicValue, 0)
        XCTAssertFalse(board.isEndPosition)
    }
    

    func assertMoves(_ moves:[ConnectMove], expectedColumns:[Int], expectedPlayer: Player) {
        
        var columns =  [Int:Int]()
        
        for column in expectedColumns {
            columns[column] = column
        }
        
        for move: ConnectMove in moves {
            XCTAssertEqual(move.player, expectedPlayer, "Move has wrong player \(move.player) ")
            XCTAssertNotNil(columns.removeValue(forKey: move.column),"Missing move for column \(move.column)")
        }
        
        XCTAssertTrue(columns.isEmpty, "Moves for columnes: \(columns) are not expected");
    }
    
}
