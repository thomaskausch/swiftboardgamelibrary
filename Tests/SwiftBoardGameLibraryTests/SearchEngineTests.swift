//
//  SearchEngineTests.swift
//  SwiftBoardGameLibrary
//
//  Created by Thomas Kausch on 13/08/14.
//  Copyright (c) 2014 Thomas Kausch. All rights reserved.
//
//
//  SwiftBoardGameLibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General   License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  SwiftBoardGameLibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General   License for more details.
//
//  You should have received a copy of the GNU General   License
//  along with SwiftBoardGameLibrary.  If not, see <http://www.gnu.org/licenses/>.
//

import XCTest
@testable import SwiftBoardGameLibrary


class SearchEngineTests: XCTestCase {

    
    var searchEngine = AlphaBetaSearchEngine<ConnectBoard>()
    var board = ConnectBoard()
    
    override func setUp() {
        super.setUp()
        board = ConnectBoard()
    }
    

    func testWinningPositionWithSearchDepthOne() {
        
        //
        // x 0
        // x 0
        // x 0
        
        for _ in 1...3 {
            board.execute(move: ConnectMove(player: .white, column: 1))
            board.execute(move: ConnectMove(player: .black, column: 2))
        }
        
        let (bestMove, boardValue) = searchEngine.bestMove(board: &board, searchDepth: 1)
        
        XCTAssertEqual(boardValue, Int.max , "White wins board value must be maxInt")
        
        board.execute(move: bestMove!)
        
        XCTAssertEqual(board.isEndPosition, true, "Board represents end position")
        XCTAssertEqual(board.winner, .white, "White must be the winner")
        XCTAssertEqual(board.heuristicValue, Int.max, "Board value mast be maximum")
        

    }
    
    func testWinningPositionWithSearchDepthTwo() {
        
        //
        //
        //       x
        //     x 0
        //     0 0
        //  x  x x 0
        
        board.execute(move: ConnectMove(player: .white, column: 1))
        board.execute(move: ConnectMove(player: .black, column: 5))
        board.execute(move: ConnectMove(player: .white, column: 3))
        board.execute(move: ConnectMove(player: .black, column: 3))
        board.execute(move: ConnectMove(player: .white, column: 4))
        board.execute(move: ConnectMove(player: .black, column: 4))
        board.execute(move: ConnectMove(player: .white, column: 3))
        board.execute(move: ConnectMove(player: .black, column: 4))
        board.execute(move: ConnectMove(player: .white, column: 4))
        
        var (bestMove, boardValue) = searchEngine.bestMove(board: &board, searchDepth: 2)
        
        XCTAssertEqual(boardValue, Int.max, "White is maximizer and will win in two moves")
        XCTAssertNotNil(bestMove, "")
        
        board.execute(move: bestMove!)
    
        (bestMove, boardValue) = searchEngine.bestMove(board: &board, searchDepth: 2)
        board.execute(move: bestMove!)
        
        XCTAssertEqual(board.isEndPosition, true, "Board represents end position")
        XCTAssertEqual(board.winner, .white, "White must be the winner")
        XCTAssertEqual(board.heuristicValue, Int.max, "Board value mast be maximum")
        
    }
    
    
}
